<?php require_once "./code.php"; ?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S4 Access Modifiers and Inheritance</title>
</head>
<body>
	<h1>Access Modifiers</h1>

	<h2>Building Variables</h2>
	<?php //$condominium->name; ?>



	<p>You should see "Cannot access protected property Condominium::$name" when trying to directly access a protected property.</p>


	<h1>Encapsulation</h1>
	<p><?= $condominium->getName(); ?></p>


	<?php //$apartment->name = "Enzo Apartment"; ?>


	<?php $apartment->setName('Enzo Apartment'); ?>
	<p>The name of the apartment has been changed to <?= $apartment->getName(); ?> </p>
</body>
</html>